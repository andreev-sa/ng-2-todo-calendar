import {Injectable} from "@angular/core";
import {CalendarTodoItem} from "../models/calendar_todo_item.model";
import {DateCode} from "../models/util/date_code";
import {TASKS_LIST} from "./mock_tasks";
import {IStorage} from "../iface/istorage";
interface ISerialisedTodo{
  text: string,
  timestampAssignedTo: number,
  isComplete: boolean
}

@Injectable()
export class TodoCalendarRecordService {
  private STORAGE_KEY = 'CalendarTodoItemsList';
  private storage: IStorage = null;
  private lastRead: CalendarTodoItem[];


  constructor(storage?: IStorage) {

    let _joinArr;
    if (storage) {
      this.storage = storage;
      let _list: Array<ISerialisedTodo> = [].concat(this.storage.get(this.STORAGE_KEY)||[]);
      _joinArr = _list.map(itm => {
        let _todo = new CalendarTodoItem(itm.text, itm.timestampAssignedTo);
        if (itm.isComplete) {
          _todo.markCompleted();
        }
        return _todo;
      })
    }
    else {
      _joinArr = TASKS_LIST;
    }
    let _arr = this.lastRead = [];
    _joinArr.map(_todo => {
      _todo.subscribeChange(type => {
        this.setList();
      });
      _arr.push(_todo);
    })
  }

  getList(filter?: any) {
    return new Promise<CalendarTodoItem[]>(resolve => {
      const FETCH_LATENCY = 150;

      setTimeout(() => {
        resolve(this.lastRead);
      }, FETCH_LATENCY);
    });
  }

  private setList(custom?: CalendarTodoItem[]) {
    let _srcList = this.lastRead;
    if (custom) {
      _srcList = custom;
    }
    if (this.storage) {
      let _newList = _srcList.filter(t => {
        return !t.wasDeleted();
      });
      let _data = _newList.map(t => {
        return {
          text: t.text,
          timestampAssignedTo: t.timestampAssignedTo,
          isComplete: t.wasCompleted()
        }
      });
      this.storage.set(this.STORAGE_KEY, _data);
    }
  }

  findRecords(code: DateCode) {
    return this.getList()
      .then(function (taskList) {
        if (taskList) {
          return taskList.filter(task => task.hasCode(code));
        }
        return [];
      })
      .then(tasks => tasks.filter(task => task.hasCode(code)));
  }

  addTask(task: CalendarTodoItem) {
    this.lastRead.push(task);
    task.subscribeChange(type => {
      this.setList();
    });
    this.setList();
  }

}
