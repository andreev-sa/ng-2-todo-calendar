import {CalendarTodoItem} from "../models/calendar_todo_item.model";
export const TASKS_LIST: CalendarTodoItem[] = [

  new CalendarTodoItem('Do it D.!', +new Date(2017, 0, 9, 12, 0)),
  new CalendarTodoItem('Do it I.!', +new Date(2017, 0, 10, 12, 0)),
  new CalendarTodoItem('Do it S.!', +new Date(2017, 0, 11, 12, 0)),
  new CalendarTodoItem('Do it C.!', +new Date(2017, 0, 12, 12, 0)),
  new CalendarTodoItem('Do it O.!', +new Date(2017, 0, 13, 12, 0)),

  new CalendarTodoItem('commit', +new Date(2017, 0, 15, 12, 0)),
  new CalendarTodoItem('push', +new Date(2017, 0, 15, 12, 1)),
  new CalendarTodoItem('run', +new Date(2017, 0, 15, 12, 2)),

  new CalendarTodoItem('Kriek', +new Date(2017, 0, 20, 19, 0)),
  new CalendarTodoItem('Fiddler\'s Green', +new Date(2017, 0, 20, 20, 0)),
  new CalendarTodoItem('Punk Brew', +new Date(2017, 0, 20, 21, 0)),
  new CalendarTodoItem('Цветочки', +new Date(2017, 0, 20, 22, 0)),
  new CalendarTodoItem('Эпсилон', +new Date(2017, 0, 20, 22, 20)),
  new CalendarTodoItem('Poison', +new Date(2017, 0, 20, 23, 50)),


  new CalendarTodoItem('Pay bills', +new Date(2017, 0, 28, 16, 30)),
  new CalendarTodoItem('Buy:\r\n' +
    'Tomato;\r\n' +
    'Bread;\r\n' +
    'Oranges;\r\n' +
    'Apples;\r\n' +
    'Milk;\r\n' +
    'Tea;\r\n' +
    ''
    , +new Date(2017, 0, 28, 23, 0))
];
