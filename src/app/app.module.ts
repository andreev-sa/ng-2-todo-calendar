import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {HeaderComponent} from "./components/header/header.component";
import {MainComponent} from "./components/main/main.component";
import {CalendarComponent} from "./components/main/calendar/calendar.component";
import {CellComponent} from "./components/main/calendar/cell/cell.component";
import {TaskComponent} from "./components/main/calendar/task/task.component";
import {CellPopoverComponent} from "./components/main/calendar/cell_popover/cell_popover.component";
import {SingleLinePreview} from "./pipes/single_line_preview.pipe";
import {LocalStorageModule} from "angular-2-local-storage";
import {PopoverModule} from "ng2-pop-over";

@NgModule({
  declarations: [
    AppComponent,

    SingleLinePreview,

    HeaderComponent, MainComponent,
    CalendarComponent, CellComponent, TaskComponent, CellPopoverComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    PopoverModule,
    LocalStorageModule.withConfig({
      prefix: 'calendar-todos',
      storageType: 'localStorage'
    })

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
