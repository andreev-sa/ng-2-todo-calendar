import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'singleLinePreview'})
export class SingleLinePreview implements PipeTransform {
  transform(fullText: string): string {
    return fullText.split('\n', 2)[0];
  }
}
