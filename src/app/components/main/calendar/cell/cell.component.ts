import {Component, Input, OnInit} from "@angular/core";
import {TodoGridItem} from "../../../../models/todo_grid.model";
import {PopOverComponent} from "ng2-pop-over/pop-over.component";

@Component({
  selector: 'calendar-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})

export class CellComponent implements OnInit {
  private popInitDone: boolean = false;
  moreText = 'more...';
  isPinRight: boolean = false;
  isPinBottom: boolean = false;
  popPosition = 'top left';
  anchorPosition = 'top right';
  @Input() col;
  @Input() row;
  @Input() cellData: TodoGridItem;

  ngOnInit() {
    let anchorPos = [], popPos = [];

    if (this.row < 4) {
      popPos.push('top');
      anchorPos.push('top');
    }
    else {
      popPos.push('bottom');
      anchorPos.push('bottom');
      this.isPinBottom = true;
    }

    if (this.col > 5) {
      popPos.push('right');
      anchorPos.push('left');
      this.isPinRight = true;
    }
    else {
      popPos.push('left');
      anchorPos.push('right');
    }

    this.popPosition = popPos.join(' ');
    this.anchorPosition = anchorPos.join(' ');
  }

  popToggle(PO: PopOverComponent, $event) {
    if (!this.popInitDone) {
      PO.toggle($event);
      PO.hide();
      setTimeout(function initShow() {
        PO.hide();
      }, 50);
      setTimeout(function initShow() {
        PO.toggle($event);
      }, 100);
      this.popInitDone = true;
      return;
    }
    PO.toggle($event);
  }

  handleTaskUpdated() {
    this.cellData.triggerUpdate();
  }
}
