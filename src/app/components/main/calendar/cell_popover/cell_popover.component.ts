import {Component, Input} from "@angular/core";
import {TodoGridItem} from "../../../../models/todo_grid.model";
let auto_grow = function auto_grow(element) {
  element.style.removeProperty('height');
  element.style.height = (element.scrollHeight) + "px";
};

@Component({
  selector: 'cell-popover',
  templateUrl: './cell_popover.component.html',
  styleUrls: ['./cell_popover.component.css']
})

export class CellPopoverComponent {
  messagePlaceholder: string = '<Ctrl> + <Enter> - add event';
  @Input() cellData: TodoGridItem;

  onSubmit() {
    // console.log(arguments);
  }

  autoGrow(messageTA: HTMLTextAreaElement) {
    auto_grow(messageTA);
  }

  onKeyUp(keyEvent: KeyboardEvent, message: string, form: HTMLFormElement) {
    auto_grow(form.elements.namedItem('message'));

    if (keyEvent && keyEvent.keyCode === 13 && keyEvent.ctrlKey && message.length) {
      this.cellData.addTask(message);
      form.reset();
    }
  }

  handleTaskUpdated() {
    this.cellData.triggerUpdate();
  }
}
