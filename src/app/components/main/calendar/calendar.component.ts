import {Component} from "@angular/core";
import {TodoCalendar} from "../../../models/todo_calendar.model";
import {TodoGrid} from "../../../models/todo_grid.model";
import {LocalStorageService} from "angular-2-local-storage";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent {
  content = 'calendar';
  b_css = 'b_calendar';
  controller: TodoCalendar;
  grid: TodoGrid;
  days: string[];

  constructor(private storage?: LocalStorageService) {

    let opts = {lang: 'en', storage: null};
    if (storage && storage.isSupported) {
      opts.storage = storage;
    }

    this.controller = new TodoCalendar(null, opts);
    this.days = this.controller.getDaysNames();
    this.refresh();


  }

  private refresh() {
    this.grid = this.controller.grid;
  }

  showNext() {
    this.controller.makeNextMonth();
    this.refresh();
  }

  showPrev() {
    this.controller.makePrevMonth();
    this.refresh();
  }
}
