import {Component, Input, Output, EventEmitter, OnInit} from "@angular/core";
import {CalendarTodoItem} from "../../../../models/calendar_todo_item.model";
// import {EventEmitter} from "@angular/common/src/facade/async";

@Component({
  selector: 'todo-task-item',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {


  deleteText: string = 'delete';
  @Output() taskUpdated = new EventEmitter();
  @Input() task: CalendarTodoItem;
  @Input() isPreview: boolean = true;

  ngOnInit() {
    this.task.subscribeChange(action => this.triggerUpdate(action));
  }

  completeTask(e) {
    e.stopPropagation();
    this.task.markCompleted();
    return false;
  }

  renewTask(e) {
    e.stopPropagation();
    this.task.markUncompleted();
    return false;
  }

  deleteTask(e) {
    e.stopPropagation();
    this.task.shadowDelete();
    return false;
  }

  private triggerUpdate(action?: any) {
    this.taskUpdated.emit(this.task);
  }
}
