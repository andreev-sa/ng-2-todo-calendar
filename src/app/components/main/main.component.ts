import {Component} from "@angular/core";

@Component({
  // moduleId: module.id,
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {
  content = 'main';
  b_css = 'b_main';
}
