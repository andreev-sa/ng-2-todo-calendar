export class DateCode {
  private code: string;
  private dateStr: string;
  private dateMs: number;

  constructor(date: Date) {
    function _leadZero(n: number) {
      return (n < 10 ? '0' : '') + n;
    }

    this.code = 'dc_' + date.getFullYear() + '.' + _leadZero(date.getMonth() + 1) + '.' + _leadZero(date.getDate());
    this.dateStr = '' + date.getDate();
    this.dateMs = date.getTime();
  }

  isEqual(DateCode) {
    return DateCode.code === this.code;
  }

  getDateString(frmt?: string) {
    return this.dateStr;
  }

  getCodeString() {
    return this.code;
  }

  getDateMs() {
    return this.dateMs;
  }
}
