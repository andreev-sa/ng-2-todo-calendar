export function getLangPack(ln?: string) {
  const mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  const wS = ["Sun", "Mon", "Tues", "Wed", "Thu", "Fri", "Sat"];

  let _default = {
    firstDay: 0,
    months: mL,
    week: wS,
  };

  let _opts = {
    'en': {
      firstDay: 1,
      months: mL,
      week: wS,
    }
  };


  if (ln && _opts.hasOwnProperty(ln)) {
    return _opts[ln];
  }

  return _default;
}

