export class CalState {
  dayNames: string[];
  pageFirstDayDate: Date;
  weeksCount: number = 0;
  monthName: string = "";
  yearStr: string = "";
  currentItemsIndexFrom: number = -1;
  currentItemsIndexTo: number = -1;
}
