import {getLangPack} from "./util/lang_pack";
import {CalState} from "./util/calendar_state";

class CalConfig {
  firstWeekDay: number;
  daysNames: string[];
  monthsArr: string[];

  selectedDate: Date;
  prevMonthDate: Date;
  nextMonthDate: Date;

  todayDate: number;
  monthNum: number;
  yearNum: number;

  getDaysNames(): string[] {
    return this.daysNames;
  }

  getMonthName(): string {
    return this.monthsArr[this.monthNum - 1] || '-';
  }

  getYearName(): string {
    return '' + this.yearNum;
  }
}

export class CalendarConfig {
  private config: CalConfig = new CalConfig();
  private state: CalState = new CalState();
  private initTimeMs: number;

  constructor(_ms?: number, opts?: any) {
    this.initTimeMs = _ms ? _ms : (new Date()).getTime();
    this.buildConfig(this.initTimeMs, opts.lang);
    this.refreshState();
  }

  private setDate(ms: number) {
    let _d = new Date(ms);

    this.config.selectedDate = _d;
    this.config.todayDate = _d.getDate();
    this.config.monthNum = _d.getMonth() + 1;
    this.config.yearNum = _d.getFullYear();
  }

  private buildConfig(ms: number, lang?: string) {
    let _langPack = getLangPack(lang);


    let _weekDays = [].concat(
      _langPack.week.slice(_langPack.firstDay),
      _langPack.week.slice(0, _langPack.firstDay)
    );
    this.config.firstWeekDay = _langPack.firstDay;
    this.config.daysNames = _weekDays;
    this.config.monthsArr = _langPack.months;
    this.setDate(ms);
  }

  private refreshState() {
    const _ms = this.config.selectedDate.getTime();
    const _firstDay = this.config.firstWeekDay;

    this.state.dayNames = this.config.getDaysNames();
    this.state.monthName = this.config.getMonthName();
    this.state.yearStr = this.config.getYearName();

    const dayMs = 24 * 60 * 60 * 1000;

    let firstMonthDate = new Date(_ms);

    while (firstMonthDate.getDate() !== 1) {
      firstMonthDate = new Date(firstMonthDate.getTime() - dayMs);
    }

    let _daysCount = 27;
    let lastMonthDate = new Date(firstMonthDate.getTime() + _daysCount * dayMs);

    while (lastMonthDate.getDate() !== 1) {
      _daysCount += 1;
      lastMonthDate = new Date(lastMonthDate.getTime() + dayMs);
    }

    this.config.prevMonthDate = new Date(firstMonthDate.getTime() - dayMs);
    this.config.nextMonthDate = lastMonthDate;

    let daysBefore = (7 + firstMonthDate.getDay() - _firstDay) % 7;
    let daysAfter = (7 - lastMonthDate.getDay() + _firstDay) % 7;

    if ((_daysCount + daysBefore + daysAfter) % 7) {
      console.error('refreshState for calendar config error: bad days count. Possible bad week count');
    }

    this.state.currentItemsIndexFrom = daysBefore;
    this.state.currentItemsIndexTo = daysBefore + _daysCount;
    this.state.weeksCount = Math.ceil((_daysCount + daysBefore + daysAfter) / 7);
    this.state.pageFirstDayDate = new Date(firstMonthDate.getTime() - daysBefore * dayMs);
  }

  getState() {
    return this.state;
  }

  private changeDate(d: Date) {
    this.setDate(d.getTime());
    // this.config.selectedDate = d;
    this.refreshState();
  }

  nextMonth() {
    this.changeDate(this.config.nextMonthDate);
  }

  prevMonth() {
    this.changeDate(this.config.prevMonthDate);
  }

}
