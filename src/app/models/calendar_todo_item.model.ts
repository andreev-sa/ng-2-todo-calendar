import {TodoItem} from "./todo_item.model";
import {DateCode} from "./util/date_code";

export class CalendarTodoItem extends TodoItem {

  private readonly timestampCreated: number;
  timestampAssignedTo: number;
  timecode: DateCode;

  constructor(message: string, date: number) {
    super(message);
    let _d = new Date(date);
    this.timestampCreated = (new Date()).getTime();
    this.timestampAssignedTo = date;
    this.timecode = new DateCode(_d);
  }

  hasCode(code: DateCode) {
    return code.isEqual(this.timecode);
  }

  shadowDelete() {
    super.shadowDelete();
  }

}
