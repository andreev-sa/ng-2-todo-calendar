// import {Output} from "@angular/core";
import {EventEmitter} from "@angular/core";
import {ITodoItem} from "../iface/itodo_item";


let count = 1;

export class TodoItem implements ITodoItem {
  text: string;

  private id: number;
  private isCompleted: boolean;
  private isDeleted: boolean;

  private taskManager: EventEmitter<any> = new EventEmitter();

  constructor(message: string) {
    this.id = count++;
    this.text = message;
    this.isCompleted = false;
    this.isDeleted = false;
  }

  markCompleted() {
    this.isCompleted = true;
    this.taskManager.emit('change');
  }

  markUncompleted() {
    this.isCompleted = false;
    this.taskManager.emit('change');
  }

  toggleCompleted() {
    this.isCompleted = !this.isCompleted;
    this.taskManager.emit('change');
  }

  shadowDelete() {
    this.isDeleted = true;
    this.taskManager.emit('del');
  }

  wasDeleted(): boolean {
    return this.isDeleted;
  }

  wasCompleted(): boolean {
    return this.isCompleted;
  }

  getId(): number {
    return this.id;
  }

  hasId(checkId: number) {
    return checkId === this.id;
  }

  subscribeChange(fn: any) {
    this.taskManager.subscribe(fn);
  }
}
