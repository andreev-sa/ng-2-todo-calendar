import {CalendarConfig} from "./calendar_config.model";
import {TodoGrid} from "./todo_grid.model";
import {CalendarModelController} from "../controllers/calendarModelController";
import {IStorage} from "../iface/istorage";

interface TodoCalendarOptions{
  lang?: string,
  storage: IStorage
}

export class TodoCalendar {
  private config: CalendarConfig;
  private controller: CalendarModelController;
  public grid: TodoGrid;

  constructor(ms?: number, opts?: TodoCalendarOptions) {
    this.config = new CalendarConfig(ms, opts);
    this.controller = new CalendarModelController(this.config, opts);
    this.grid = this.controller.getGrid();
  }

  getDaysNames() {
    return this.config.getState().dayNames;
  }

  //
  // getTitle(){
  //   let _state = this.config.getState();
  //   return _state.monthName + " " + _state.yearStr;
  // }

  makeNextMonth() {
    this.controller.nextMonth();
    this.grid = this.controller.getGrid();

  }

  makePrevMonth() {
    this.controller.prevMonth();
    this.grid = this.controller.getGrid();
  }
}
