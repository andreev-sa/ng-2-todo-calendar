import {TodoCalendarRecordService} from "../services/todo_calendar_records.service";
import {CalendarTodoItem} from "./calendar_todo_item.model";
import {DateCode} from "./util/date_code";
import {CalState} from "./util/calendar_state";
import {IStorage, IStorageOptions} from "../iface/istorage";


let TASK_MAX_COUNT = 3;

class AimCross {
  row: number;
  col: number;
}
class ItemConfig {
  dateCode: DateCode;
  isCurrent: boolean;
  isToday: boolean
}

class TodoGridItem {
  service: TodoCalendarRecordService;
  dateCode: DateCode;
  isCurrentMonth: boolean = false;
  isToday: boolean = false;
  tasks: CalendarTodoItem[] = [];
  tasksVisible: CalendarTodoItem[] = [];

  constructor(config: ItemConfig, service: TodoCalendarRecordService) {
    let dateCode = config.dateCode;
    const isCurrent = config.isCurrent, isToday = config.isToday;

    this.service = service;

    let _self = this;
    this.dateCode = dateCode;
    this.isCurrentMonth = isCurrent;
    this.isToday = isToday;
    let _allTasksList = this.tasks = [];
    this.tasksVisible = [];
    service.findRecords(dateCode).then(function (todos) {
      if (todos) {
        todos.map(t => !t.wasDeleted() && _allTasksList.push(t));
        _self.triggerUpdate();
      }

    });
  }

  private tasksRefresh() {
    let _allTasksList = this.tasks;
    let _copy = [].concat(_allTasksList);
    _allTasksList.length = 0;
    if (_copy.length) {
      _copy.map(t => !t.wasDeleted() && _allTasksList.push(t));
    }
    _allTasksList.sort((a, b) => {
      return a.wasCompleted() && !b.wasCompleted() ? 1 : 0
    })
  }

  private visibleRefresh() {
    let _tasksVisibleList = this.tasksVisible;
    _tasksVisibleList.length = 0;
    if (this.tasks.length) {
      this.tasks.map(t => !t.wasDeleted() &&
        _tasksVisibleList.length < TASK_MAX_COUNT &&
        _tasksVisibleList.push(t)
      );
    }
  }

  public triggerUpdate() {
    this.tasksRefresh();
    this.visibleRefresh();
  }

  addTask(message: string) {
    let _itm = new CalendarTodoItem(message, this.dateCode.getDateMs());
    this.service.addTask(_itm);
    this.tasks.push(_itm);
    this.triggerUpdate();

  }

  deleteTask(task: CalendarTodoItem) {
    task.shadowDelete();
    this.triggerUpdate();
  }
}

class TodoGridSet {
  items: TodoGridItem[];

  constructor(aim: AimCross, service: TodoCalendarRecordService, state: CalState) {
    let firstDate = state.pageFirstDayDate;
    this.items = [];

    const itemsCount = aim.col;
    const rowShift = aim.row;
    const dayMs = 24 * 60 * 60 * 1000;
    let iter = 0;

    while (itemsCount !== iter) {
      const _index = (iter + rowShift * itemsCount);
      const isCurrent = state.currentItemsIndexFrom <= _index && state.currentItemsIndexTo > _index;
      const _d = new Date(firstDate.getTime() + dayMs * _index);
      const _dc = new DateCode(_d);
      const _isToday = _dc.isEqual(new DateCode(new Date()));
      let _cnfg = {dateCode: _dc, isCurrent: isCurrent, isToday: _isToday};
      this.items.push(new TodoGridItem(_cnfg, service));
      iter += 1;
    }
  }
}

class TodoGrid {
  private storage: IStorage = null;
  private service: TodoCalendarRecordService = null;
  title: string;
  rows: TodoGridSet[];

  constructor(state: CalState, opts?: IStorageOptions) {
    if (opts && opts.storage) {
      this.storage = opts.storage;
    }
    this.service = new TodoCalendarRecordService(this.storage);

    let rowCount = state.weeksCount;
    let itemsCount = state.dayNames.length;
    this.rows = [];
    this.title = state.monthName + " " + state.yearStr;
    let shift = 0;
    while (rowCount--) {
      this.rows.push(new TodoGridSet({col: itemsCount, row: shift++}, this.service, state));
    }
  }
}

export {TodoGridItem, TodoGridSet, TodoGrid}
