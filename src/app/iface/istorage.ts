export interface IStorage {
  isSupported: boolean;
  get<T>(key: string): T;
  keys(): Array<string>;
  length(): number;
  remove(...keys: Array<string>): boolean;
  set(key: string, value: any): boolean;
}

export interface IStorageOptions{
  storage?: IStorage;
}
