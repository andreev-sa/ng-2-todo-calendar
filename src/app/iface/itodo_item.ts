export interface ITodoItem {
  text: string;

  markCompleted(): void;
  markUncompleted(): void;
  shadowDelete(): void;
  wasDeleted(): boolean;
  wasCompleted(): boolean;
}
