import {CalendarConfig} from "../models/calendar_config.model";
import {TodoGrid} from "../models/todo_grid.model";
import {IStorageOptions} from "../iface/istorage";



export class CalendarModelController {
  config: CalendarConfig;
  options: IStorageOptions;

  constructor(config: CalendarConfig, opts?: IStorageOptions) {
    this.config = config;
    this.options = opts;
  }

  getGrid(): TodoGrid {
    return new TodoGrid(this.config.getState(), this.options);
  }

  nextMonth() {
    this.config.nextMonth();
  }

  prevMonth() {
    this.config.prevMonth();
  }
}
